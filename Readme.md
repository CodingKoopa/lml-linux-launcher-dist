# Lucas' Simpsons Hit & Run Mod Launcher Linux Packaging
This repository contains distribution-specific packaging for the [Lucas' Simpsons Hit & Run Mod Launcher Linux Launcher](https://gitlab.com/CodingKoopa/lml-linux-launcher).

## Usage
Using this repository:
1. Clone this repo.
```sh
git clone git@gitlab.com:CodingKoopa/lml-linux-launcher-dist.git
```
2. Update the submodule you're working with, if applicable:
```sh
git submodule update $SUBMODULE
```
3. If you are working with a submodule, fix the detached HEAD:
```sh
git checkout master
```

## `pkgbuild` (Arch Linux)
You may view this repository online [on the AUR](https://aur.archlinux.org/cgit/aur.git/?h=lucas-simpsons-hit-and-run-mod-launcher).

The package may be linted like so:
1. Lint the PKGBUILD as a Bash script:
```
shellcheck PKGBUILD
```
2. Check the PKGBUILD via namcap:
```sh
namcap --exclude=carch PKGBUILD
``` 

The update procedure for maintainers is as follows:
1. Update `pkgver` and/or `pkgrel`.
2. Update the source checksums.
```sh
updpkgsums
```
3. Fix the formatting in the `PKGBUILD`.
4. Updage the `.SRCINFO`.
```sh
makepkg --printsrcinfo > .SRCINFO
```
